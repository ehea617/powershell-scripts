<#*******************************************************************************
    
    Copyright (C) 2016 Evan Amara
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

******************************************************************************#>

#Requires -RunAsAdministrator

# Get the location of the virtual hard disk in the root directory of the script.
$vhd = Get-Item $PSScriptRoot/*.vhdx

# Get the file name of the virtual hard disk to be copied.
$copiedvhd = $vhd | Split-Path -Leaf

# Get the amount of RAM per memory module.
$memory = Get-WmiObject -Class win32_physicalmemory

# Get the total amount of RAM in bytes by multiplying the number of modules by the capacity of a single module. This needs to be redone to take into account different capacity RAM modules.
$memorytotal = $memory.Capacity[0]*$memory.Capacity.Length

# Get 1/4 the total amount of RAM in bytes.
[int64]$memoryvm = $memorytotal*0.25

# Get the processor info.
$cores = Get-WmiObject -Class win32_processor

# Get the total number of logical processors (cores).
$corestotal = $cores.NumberOfLogicalProcessors

# Get 1/2 the total number of logical processors (cores).
$coresvm = $corestotal*0.5

# Get the external virtual switch (if there is one).
$externalvswitch = Get-VMSwitch -SwitchType External

Write-Host "Checking for pre-existing external virtual switch."

# Check for pre-existing external virtual switch.
if($externalvswitch.SwitchType -eq "External") {
    Write-Host "External virtual switch already exists."

    # Set the virtual machine to use the pre-existing virtual switch. Do not attempt to make a new one.
    [string]$vswitchvm = $externalvswitch.Name
} else {
    Write-Host "No external virtual switch found. Creating one."
    
    # Get the Ethernet adapter and store it in $ethernet. Todo: Add wireless adapter as a fallback if Ethernet adaper doesn't exist and make sure that there is at least one network adapter to use.
    $ethernet = Get-NetAdapter -Name "Ethernet"
    
    # Make a new external virtual switch.
    New-VMSwitch -Name autoExternalSwitch -NetAdapterName $ethernet.Name -AllowManagementOS $true
    
    # Set the virtual machine to use the auto generated external virtual switch.
    $vswitchvm = "autoExternalSwitch"
}

# Amount of virtual machines to be created. Todo: input validation.
[int]$vmamount = Read-Host "How many Virtual Machines do you want to create?"

# Prefix appended to virtual machine names. Todo: input validation.
[string]$vmprefix = Read-Host "What prefix do you want to use for your Virtual Machines?"

# Internal counter to cross check $vmamount.
$counter = 1

# Make virtual machines when called.
function Make-VMs {
    if($counter -le $vmamount) {
        Write-Host "Creating Virtual Machine $counter."

        # Make basic virtual machine folder structure.
        New-Item -Path "$PSScriptRoot/$vmprefix-$counter" -ItemType directory
        New-Item -Path "$PSScriptRoot/$vmprefix-$counter/Virtual Hard Disks" -ItemType directory
        New-Item -Path "$PSScriptRoot/$vmprefix-$counter/Virtual Machines" -ItemType directory

        # Copy virtual hard disk to Virtual Hard Disks folder.
        Copy-Item -Path $vhd -Destination "$PSScriptRoot/$vmprefix-$counter/Virtual Hard Disks/"

        # Rename copied virtual hard disk.
        Rename-Item -Path "$PSScriptRoot/$vmprefix-$counter/Virtual Hard Disks/$copiedvhd" -NewName "$vmprefix-$counter.vhdx"

        # Create the virtual machine.
        New-VM -Name "$vmprefix-$counter" -MemoryStartupBytes $memoryvm -Generation 1 -VHDPath "$PSScriptRoot/$vmprefix-$counter/Virtual Hard Disks/$vmprefix-$counter.vhdx" -BootDevice VHD -Path "$PSScriptRoot/$vmprefix-$counter/Virtual Machines" -SwitchName $vswitchvm
        Set-VMProcessor "$vmprefix-$counter" -Count $coresvm

        # Increment the internal counter by 1.
        $counter = $counter + 1

        # Run function again
        Make-VMs
    } else {
        # End script.
        Write-Host "Done."
    }
}

# Run function to make virtual machines.
Make-VMs