# hyperv-powershell-scripts
Hyper-V automation with PowerShell.

## setup-hyper-v.ps1
Enables Hyper-V if it is not already enabled. Creates external, internal, and private virtual switches for networking.

## make-vms.ps1
Creates virtual machines using a virtual hard disk in the same location as the PowerShell script. Uses 1/4 the total amount of RAM in the system and 1/2 the total amount of logical processors per virtual machine.

## License
[GPLv3](https://gitlab.com/ehea617/hyperv-powershell-scripts/blob/master/LICENSE.txt)
