﻿<#*******************************************************************************
    
    Copyright (C) 2016 Evan Amara
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

******************************************************************************#>

#Requires -RunAsAdministrator

# Enable Hyper-V when called.
function Enable-Hyper-V {
    Write-Host "Enabling Hyper-V."
    Enable-WindowsOptionalFeature -FeatureName Microsoft-Hyper-V-All -Online -NoRestart
}

# Configure Hyper-V when called.
function Configure-Hyper-V {
    # Get the Ethernet adapter and store it in $ethernet. Todo: Add wireless adapter as a fallback if Ethernet adaper doesn't exist and make sure that there is at least one network adapter to use.
    $ethernet = Get-NetAdapter -Name "Ethernet"

    New-VMSwitch -Name autoExternalSwitch -NetAdapterName $ethernet.Name -AllowManagementOS $true
    New-VMSwitch -Name autoInternalSwitch -SwitchType Internal
    New-VMSwitch -Name autoPrivateSwitch -SwitchType Private

    Write-Host "Hyper-V configured successfully."
}

function Check-VSwitch {
    Write-Host "Checking for Virtual Switches."

    # Get the currently relevant virtual switches. Todo: see comment below
    $switches = Get-VMSwitch -Name "*Switch"
    # Check if virtual switches already exist before creating new ones. This needs to be redone because only one external virtual switch can exist per network adapter.
    if($switches.Name -eq "autoPrivateSwitch" -or $switches.Name -eq "autoInternalSwitch" -or $switches.Name -eq "autoExternalSwitch") {
        Write-Host "Virtual switches exist already."
    } else {
        Configure-Hyper-V
    }
}

Write-Host "Checking to see if Hyper-V is already enabled."

# Get the Hyper-V feature and store it in $hyperv.
$hyperv = Get-WindowsOptionalFeature -FeatureName Microsoft-Hyper-V-All -Online

# Check if Hyper-V is already enabled. If it isn't, enable it.
if($hyperv.State -eq "Enabled") {
    Write-Host "Hyper-V is already enabled."
    Check-VSwitch
} else {
    Write-Host "Hyper-V is disabled. Enabling."
    Enable-Hyper-V
    Write-Host "Configuring Hyper-V."
    Configure-Hyper-V
    # Restart computer to complete Hyper-V install.
    Restart-Computer
}